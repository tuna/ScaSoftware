A brief introduction to Henk's JTAG code in SCA-SW ...

In June 2018, the team took (following an agreement, of course) Henk's JTAG algorithm
and integrated it into SCA-SW. 

This module of SCA-SW, even though integrated into SCA-SW, is slightly different 
from the rest; e.g. it uses its own defined (command IDs etc) rather than SCA-SW's one.

The general author of the concept is Henk Boterenbrood.
The person moving and integrating the contribution is Piotr Nikiel.
