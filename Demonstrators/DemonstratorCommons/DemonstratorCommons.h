/*
 * DemonstratorCommons.h
 *
 *  Created on: 13 Dec 2018
 *      Author: pnikiel
 */

#ifndef DEMONSTRATORS_DEMONSTRATORCOMMONS_DEMONSTRATORCOMMONS_H_
#define DEMONSTRATORS_DEMONSTRATORCOMMONS_DEMONSTRATORCOMMONS_H_

#include <string>

namespace ScaSwDemonstrators
{

void initializeLogging (const std::string& logLevel);

constexpr char helpForAddress[] =
        "SCA address, one of: \n\n"
        "sca-simulator://<1> (for pure simulation) \n\n"
        "simple-netio://direct/<hostname>/<portFromHost>/<portToHost>/<elink> (via felixcore, same elink for TX and RX; elink in hex) \n\n"
        "simple-netio://direct/<hostname>/<portFromHost>/<portToHost>/<elinkTx>/<elinkRx> (via felixcore, elinks differ for TX and RX; elinks in hex) \n\n"
        "Triangle brackets <> indicate variable parts\n\n";

}

#endif /* DEMONSTRATORS_DEMONSTRATORCOMMONS_DEMONSTRATORCOMMONS_H_ */
