/*
 * HdlcFrameTranslator.h
 *
 *  Created on: Oct 22, 2019
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 */

#pragma once

#include <vector>
#include <NetIoSimpleBackend/HdlcBackend.h>
#include <inttypes.h>

namespace NetIoSimpleBackend
{

enum HdlcFrameTypeSize
{
	HDLC_U_FRAME_SIZE = 4,
	HDLC_SREJ_FRAME_SIZE = 6
};

typedef std::vector<uint8_t>::size_type HdlcFrameSize;

class HdlcFrameTranslator
{
public:
	//! Constructs the translator.
	HdlcFrameTranslator (
	        const std::vector<uint8_t>& hdlcFrame,
	        std::shared_ptr<const SpecificAddress>& specificAddress);

	uint8_t getAddress() const { return m_address; }
	uint8_t getControl() const { return m_control; }
	uint16_t getRange() const { return m_range; }
	uint16_t getFcs() const { return m_fcs; }
	bool getFrameFcsValidity() const { return m_isFcsValid; }

private:
	uint8_t m_address;
	uint8_t m_control;
	uint16_t m_range;
	uint16_t m_fcs;
	bool m_isFcsValid;
	HdlcFrameSize m_hdlcFrameTypeSize;

	std::shared_ptr<const SpecificAddress> m_specificAddress;

	bool validateAddress ();
	bool validateControlRange ();

};

}
