/*
 * I2C.h
 *
 *  Created on: May 26, 2017
 *      Author: Paris Moschovakos
 */


#ifndef SCASIMULATOR_I2C_H_
#define SCASIMULATOR_I2C_H_

#include <ScaSimulator/ScaChannel.h>



namespace ScaSimulator
{

class I2C: public ScaChannel
{
public:

	I2C( unsigned char channelId, HdlcBackend *myBackend );
	virtual ~I2C ();

	virtual void onReceive( const Sca::Request &request );
	uint16_t m_divider = 0;
};

};

#endif /* SCASIMULATOR_I2C_H_ */
