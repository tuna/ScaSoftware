
#ifndef HDLCBACKEND_H
#define HDLCBACKEND_H

#include <Hdlc/Backend.h>
#include <Hdlc/LocalStatistician.h>

#include <boost/chrono.hpp>
#include <boost/signals2.hpp>

#include <string>
#include <map>
#include <list>
#include <mutex>
#include <thread>

namespace ScaSimulator
{

class ScaChannel;

class HdlcBackend : public Hdlc::Backend
{
public:
	HdlcBackend (const std::string& specificAddress);
	virtual ~HdlcBackend ();

	virtual const std::string& getAddress ( ) const { return m_specificAddress; }

	virtual void send (const Hdlc::Payload &request );
	virtual void send (
	          const std::vector<Hdlc::Payload> &requests,
	          const std::vector<unsigned int> times
	      );

	virtual void sendHdlcControl (uint8_t hdlcControl) {}

	virtual void subscribeReceiver ( Hdlc::Backend::ReceiveCallBack f ) { m_replyCame.connect(f); }

	virtual Hdlc::BackendStatistics getStatistics () const { return m_statistician.toBackendStatistics(); }

	void connectChannel( unsigned char channelId, ScaChannel * channel );

	void storeReply( const Hdlc::Payload& reply, boost::chrono::microseconds t );

	void tick();
	void tickThread();

	uint32_t getScaId () const { return m_scaId; }

private:
	std::string m_specificAddress;
	uint32_t m_scaId; // SCA ID of this simulated SCA will match the number passed in the specific address

	std::map<unsigned char, ScaChannel*> m_channelMap;
	uint32_t m_channelEnabledMask;


	struct TimeBoundReply
	{
		Hdlc::Payload payload;
		boost::chrono::steady_clock::time_point when;

		TimeBoundReply(const Hdlc::Payload& p, const boost::chrono::steady_clock::time_point& w):
			payload(p), when(w) {}
	};

	std::list<TimeBoundReply> m_pendingReplies;

	boost::signals2::signal<void(const Hdlc::Payload&)> m_replyCame;

	void handleNodeRequest( const Hdlc::Payload &frame );

	Hdlc::LocalStatistician m_statistician;

	std::mutex m_accessLock;

	std::thread m_simThread;

	bool m_keepRunning;


};
} // end of package namespace

#endif // HDLCBACKEND_H
