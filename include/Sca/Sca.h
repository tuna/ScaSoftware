/*
 * Sca.h
 *
 *  Created on: May 11, 2016
 *      Author: pnikiel,aikoulou, pmoschov
 */

#ifndef SCA_SCA_H_
#define SCA_SCA_H_

#include <string>
#include <memory> // for unique_ptr
#include <Sca/SynchronousService.h>

#include <Sca/Defs.h>
#include <Hdlc/Backend.h>  // to access backend statistics

#include <Sca/ScaI2c.h>
#include <Sca/ScaAdc.h>
#include <Sca/ScaDac.h>
#include <Sca/ScaGpio.h>
#include <Sca/ScaSpi.h>


namespace Hdlc
{
class Backend;
}

namespace Sca
{
	// move it to impl specific files? no reason the user would need it
	void throwIfScaReplyError(const Reply& reply);

    class Sca
    {
    public:

	Sca( const std::string& address );
	virtual ~Sca();

	Sca(const Sca& other) = delete;
	Sca& operator= (const Sca& other) = delete;

	Adc& adc() { return m_adc; }
	Gpio& gpio() { return m_gpio; }
	Spi& spi() { return m_spi; }
	I2c& i2c() { return m_i2c; }
	Dac& dac() { return m_dac; }

	uint8_t getNodeControlCommand ( int scaChannel, bool forWrite );

	void setChannelEnabled ( int scaChannel, bool enabled );
	bool getChannelEnabled ( int scaChannel );

	//! Returns true if given SCA is considered to pong
	bool ping();
	//! Just does HDLC reconnect, without resetting given SCA
	void reconnect();
	//! Hard-reset
	void reset();

	// this one really invokes read transaction
	uint32_t readChipId ();

	// this one just reads the cached value read at start-up. note sca id rather don't change ;-)
	uint32_t getChipId () const { return m_cachedScaId; }

	Hdlc::BackendStatistics getBackendStatistics () const ;

	SynchronousService& synchronousService() { return m_synchronousService; }

    private:
	std::unique_ptr<Hdlc::Backend> m_backend;
	SynchronousService m_synchronousService;
	Adc m_adc;
	Spi m_spi;
	Gpio m_gpio;
	Dac m_dac;
	I2c m_i2c;

	uint32_t m_cachedScaId;

	void enableAllChannels();

    };

} /* namespace Sca */

#endif /* SCA_SCA_H_ */
