/*
 * GpioImpl.h
 *
 *  Created on: Mar 23, 2017
 *      Author: Aimilios Koulouris, Paris Moschovakos
 */

#pragma once

#include <bitset>

namespace Sca
{

	class Gpio
	  /*!
	    NOTE: our GPIO API doesn't use event-mode notifications.
	   */
	{
	private:
		//LOW-LEVEL
		uint32_t getReadCommandFromRegister(uint32_t gpioRegister);
		uint32_t getWriteCommandFromRegister(uint32_t gpioRegister);
		void sendWriteDataToRegister(uint32_t data,uint32_t gpioRegister);
		uint32_t sendReadRegister(uint32_t gpioRegister);

	public:
		Gpio (SynchronousService& ss): m_synchronousService(ss) {}

		//USER LEVEL (using low-level functions)
		//for registers
		uint32_t getRegister(uint32_t gpioRegister);
		void setRegister(uint32_t gpioRegister,uint32_t data);

		bool getRegisterBitValue(uint32_t gpioRegister,int pinIndex);
		void setRegisterBitToValue(uint32_t gpioRegister, int pinIndex, bool setValue);

		std::vector<bool> getRegisterRangeValue(uint32_t gpioRegister,int fromPin,int toPin);
		void setRegisterRangeToValue(uint32_t gpioRegister, int fromPin,int toPin,bool setValue);

		/*
		 * Paris: The motivation of the following methods is to allow user 
		 * classes, like the GPIO BitBanger in the SCA OPC UA server, to make  
		 * requests in order to dispatch them in a single group (OPCUA-1562)
		 */

		enum class Direction
		{
			INPUT,
			OUTPUT
		};

		Request makeSetPinsValuesRequest( uint32_t registerData, std::map<unsigned int, bool> pinValue );
		Request makeGetPinsValuesRequest();
		Request makeSetPinsDirectionsRequest( uint32_t registerData, std::map<unsigned int, Direction> pinDirection );


	private:
		SynchronousService& m_synchronousService;

	};

}
