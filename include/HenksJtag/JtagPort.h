#ifndef JTAGPORT_H
#define JTAGPORT_H

#include <Sca/Request.h>
#include <Sca/Sca.h>

namespace HenksJtag
{
  class JtagPort
  {
  public:

    // States of the JTAG statemachine
    typedef enum {
      SELECT_DR_SCAN = 0,
      CAPTURE_DR,
      SHIFT_DR,
      EXIT1_DR,
      PAUSE_DR,
      EXIT2_DR,
      UPDATE_DR,

      TEST_LOGIC_RESET,
      RUN_TEST_IDLE,

      SELECT_IR_SCAN,
      CAPTURE_IR,
      SHIFT_IR,
      EXIT1_IR,
      PAUSE_IR,
      EXIT2_IR,
      UPDATE_IR
    } state_t;

    JtagPort( Sca::Sca& sca, unsigned int maxGroupSize );
    ~JtagPort();

  public:
    void    configure     ( int freq_mhz );
    void    gotoState     ( state_t s );
    state_t state         ( )         { return _state; }

    void    shiftIr       ( int nbits, uint8_t *tdo );
    void    shiftIr       ( int irlen, int instr,
                            int devs_before, int ibits_before,
                            int devs_after,  int ibits_after,
                            int dev_instr = -1,
                            bool read_tdi = false );

    void    shiftDr       ( int nbits, uint8_t *tdo,
                            bool read_tdi = false );
    void    shiftDr       ( int nbits, uint8_t *tdo, bool read_tdi,
                            int devs_before, int devs_after );

    void    shift         ( state_t state, int nbits,
                            uint8_t *tdo  = 0,
                            bool read_tdi = false,
                            bool final    = true ); // Applies to SHIFT_IR/DR only

    void    showProgress  ( bool b )  { _progress = b; }

    std::vector<Sca::Reply> getReadBackFrames( ) const { return m_readBackFrames; }
    void clearReadBack( ) { m_readBackFrames.clear(); }

  private:
    int     setState      ( state_t s, uint32_t *tms );

    void    addResetTdoTms( );

    void    mapInt2Bytes  ( uint32_t i, uint8_t *bytes );

    void    displayPercentage( int *percentage,
                               int  bytes_done,
                               int  bytes_todo );

  private:
    bool    addScaFrame    ( int chan, int len, int cmd, uint8_t *data,
                             unsigned int delay_us = 0,
                             bool store_readback = false );
    bool    uploadScaFrames( );
    void    resetScaFrames ( );

  private:
    // For JTAG
    state_t    _state;
    bool       _progress; // Whether or not to display progress in shiftDr()
    uint32_t   _ctrlReg;
    int        _goDelayFactor;

  private:
    Sca::Sca&                 m_sca;
    Sca::SynchronousService&  m_synchronousService;
    unsigned int              m_maxGroupSize;
    std::vector<Sca::Request> m_groupedRequests;
    std::vector<Sca::Reply>   m_readBackFrames;
  };
}
#endif // JTAGPORT_H
