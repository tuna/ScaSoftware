
#ifndef BACKENDFACTORY_H
#define BACKENDFACTORY_H

#include <Hdlc/Backend.h>

#include <boost/thread/recursive_mutex.hpp>

#include <string>
#include <map>

namespace Hdlc {

class BackendFactory
{
public:



  /**
   * @return Hdlc::Backend *
   * @param  scaAddress
   */
  static Backend * getBackend (const std::string& scaAddress ) ;

  static void unregister (Backend* backend);

private:
  static boost::recursive_mutex m_accessLock;
  static std::map<std::string, Backend*> m_openedBackends;
};
} // end of package namespace

#endif // BACKENDFACTORY_H
