/*
 * AwaitedRepliesController.cpp
 *
 *  Created on: 26 Oct 2017
 *      Author: pnikiel
 */


#include <Sca/AwaitedRepliesController.h>

namespace Sca
{

/*
 * Requirement: the requests given to this function must already have the intended TIDs reserved.
 */
void AwaitedRepliesController::prepare (
        std::vector<Request>& requests,
        std::vector<Reply>* replies)
{
    clear();
    // create expected entries
    for (unsigned int i=0; i<requests.size(); ++i)
    {
        Request& request = requests[i];
        if (request.transactionId() >= m_slots.size())
            scasw_throw_runtime_error_with_origin("logic-error: this transaction id can't fit the slot vector");
        m_slots[request.transactionId()].state = SlotState::Expected;
        m_slots[request.transactionId()].replyIndex = i;
    }
    m_replies = replies;
    m_stillToGo = requests.size();
}

bool AwaitedRepliesController::onReceive ( const Reply& reply )
// returns true if and only-if all replies for this request have been collected
{
    unsigned int tid = reply.transactionId();
    if (tid >= m_slots.size())
        scasw_throw_runtime_error_with_origin("logic-error: received transaction id is bigger than previsioned for this solution");
    if (m_slots[tid].state == SlotState::Expected)
    { // OK so far
        (*m_replies)[ m_slots[tid].replyIndex ] = reply;
        m_slots[tid].state = SlotState::Received;
        m_stillToGo--;

    }
    else
        LOG(Log::DBG) << "obtained transaction id was not expected";
    return m_stillToGo == 0;
}

}

