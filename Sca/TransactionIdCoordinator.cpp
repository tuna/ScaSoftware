/*
 * TransactionIdCoordinator.cpp
 *
 *  Created on: May 10, 2016
 *      Author: pnikiel
 */

#include <Sca/TransactionIdCoordinator.h>

#include <boost/thread/locks.hpp>

#include <algorithm>

namespace Sca
{

// TODO: the TIDs should be circular or in any case different... improve the following such that next time the search is started from the last successful allocated +1

bool TransactionIdCoordinator::searchInRange(unsigned int from, unsigned int to, unsigned int* output)
{
	// find first entry in the vector which is free
	auto iter = std::find( m_tidBitmap.begin() + from, m_tidBitmap.begin() + to, false );
	if (iter != m_tidBitmap.end())
	{
		// managed to find empty tid -- mark it as used
		*iter = true;
		*output = (iter - m_tidBitmap.begin()) + m_firstTid;
		return true;
	}
	else
		return false;

}

bool TransactionIdCoordinator::allocateTid(unsigned int* output)
{
	// we definitely want it to be synchronized
	boost::lock_guard<decltype(m_uniqueAccessMutex)> lock( m_uniqueAccessMutex );
	m_startSearchWith = (m_startSearchWith + 1) % m_numAvailableTids;
	if (searchInRange(m_startSearchWith, m_numAvailableTids, output))
		return true; // free TID found "on the right"
	else
		return searchInRange(0, m_startSearchWith, output);
}

void TransactionIdCoordinator::freeTid(unsigned int tid)
{
	// we definitely want it to be synchronized
	boost::lock_guard<decltype(m_uniqueAccessMutex)> lock( m_uniqueAccessMutex );
	// will throw if out-of-range, so it is safe
	m_tidBitmap.at( tid-m_firstTid ) = false;
}

}


