#include <Sca/Frames.h>

#include <ScaCommon/except.h>

Hdlc::Payload Sca::Frames::makeRequestFrame (unsigned char channelId, unsigned char command, std::vector<uint8_t>& data )
{
	if (data.size()!=0 && data.size()!=2 && data.size()!=4)
		scasw_throw_runtime_error_with_origin("data size of 0,2,4 bytes allowed only.");

	std::vector<uint8_t> frameDate ({
		0, /*transaction Id, to be filled when sending this frame */
		channelId,
		(uint8_t)data.size(),
		command
	});

	frameDate.insert( frameDate.end(), data.begin(), data.end() );

	return Hdlc::Payload( frameDate );

}


