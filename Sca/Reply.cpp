/*
 * Reply.cpp
 *
 *  Created on: May 5, 2016
 *      Author: pnikiel
 */

#include <Sca/Reply.h>
#include <ScaCommon/except.h>

namespace Sca
{

	Reply::Reply( const Hdlc::Payload& other):
			Hdlc::Payload(other)
	{
		if (size()<4)
			THROW_WITH_ORIGIN(std::runtime_error,"Frame too small to be considered a proper SCA reply");
	}

	Reply::Reply(
		uint8_t channel,
		uint8_t error,
		std::initializer_list<uint8_t> data) :
		Hdlc::Payload( {0, channel, error, (uint8_t)(4+data.size()) }, data )

{
}

	void Reply::assignTransactionId (uint8_t transactionId)
	{
		m_data[0] = transactionId;
	}

	std::string Reply::toString() const
	{
		std::ostringstream out;
		out << "[tr_id=" << (unsigned int)transactionId() << std::hex << " ch=" << (unsigned int)channelId();
		out << " err=" << (unsigned int)error() << " len=" << (unsigned int)m_data[3];
		out << " data=<";
		for (unsigned int i=4; i<m_dataSize; i++)
			out << (unsigned int)m_data[i] << " ";
		out << ">]";
		return out.str();
	}

}
