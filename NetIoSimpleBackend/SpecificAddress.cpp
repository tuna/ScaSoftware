/*
 * SpecificAddress.cpp
 *
 *  Created on: Mar 15, 2017
 *      Author: pnikiel
 */

#include <sstream>

#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>

#include <NetIoSimpleBackend/SimpleNetioSpecificAddress.h>
#include <ScaCommon/except.h>
#include <ScaCommon/ScaSwLogComponents.h>

#include <LogIt.h>

using Sca::LogComponentLevels;

namespace NetIoSimpleBackend
{

SpecificAddress::SpecificAddress (const std::string& stringAddress):
        m_originalAddress(stringAddress),
		m_elinkIdTx(0), // is parsed in the body
		m_elinkIdRx(0)
{
	// Piotr: you can use regex101.com to help yourself designing the regex
	boost::regex directAddressRegex( "^direct\\/"
			"(?<address>[a-zA-Z0-9][a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)\\/"
			"(?<port_to_sca>\\d+)\\/"
			"(?<port_from_sca>\\d+)\\/"
			"(?<elink_sca>[0-9a-fA-F]+)"
	        "(?:\\/(?<elink_sca_rx>[0-9a-fA-F]+))?" // this part is optional! In case there are different rx/tx elinks
	        "$"
			);
	boost::smatch matchResults;
	bool matched = boost::regex_match( stringAddress, matchResults, directAddressRegex );
	if (!matched)
	{
		THROW_WITH_ORIGIN(std::runtime_error,
				"supplied specific address didn't match the regex for direct address. Given string was: '"+
				stringAddress+"' Here is an example: 'direct/pcatlnswfelix01.cern.ch/12340/12345/BF'");
	}
	m_felixHostname = matchResults["address"];
	m_portToSca = boost::lexical_cast<unsigned int>(matchResults["port_to_sca"]);
	m_portFromSca = boost::lexical_cast<unsigned int>(matchResults["port_from_sca"]);
	// elinks should be given in hex, that's why lexical_cast can't be used
	std::stringstream ss;
	ss.str(matchResults["elink_sca"]);
	ss >> std::hex >> m_elinkIdTx;
	if (ss.fail())
		THROW_WITH_ORIGIN(std::runtime_error,"Didn't manage to read hex number from: "+ss.str());
	if (matchResults["elink_sca_rx"].matched)
	{
	    ss.clear();
	    ss.str(matchResults["elink_sca_rx"]);
	    ss >> std::hex >> m_elinkIdRx;
	    if (ss.fail())
	        THROW_WITH_ORIGIN(std::runtime_error,"Didn't manage to read hex number from: "+ss.str());
	    LOG(Log::INF, LogComponentLevels::netio()) << "Note: this SCA uses different elinks for TX and RX (this is a bit unusual!) TX: 0x" <<
	            std::hex << m_elinkIdTx <<
	            " RX: 0x" << std::hex << m_elinkIdRx;
	}
	else
	    m_elinkIdRx = m_elinkIdTx; // the usual situation: same elink ID for Tx and Rx
}

}



