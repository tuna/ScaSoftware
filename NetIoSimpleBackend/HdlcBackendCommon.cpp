/*
 * HdlcBackendCommon.cpp
 *
 *  Created on: Oct 25, 2019
 *      Author: Paris Moschovakos (paris.moschovakos@cern.ch)
 *      		Piotr Nikiel (pnikiel@cern.ch)
 *
 *      Description : Common utility functions used by netIO HDLC backend
 */

#include <NetIoSimpleBackend/HdlcBackendCommon.h>

#include <string>
#include <vector>
#include <iostream>
#include <ScaCommon/ScaSwLogComponents.h>
#include <felixbase/client.hpp>
#include <hdlc_coder/hdlc.hpp>

using Sca::LogComponentLevels;

namespace NetIoSimpleBackend
{

std::string vectorAsHex (const std::vector<uint8_t> & v )
{

    std::stringstream ss;
    for (uint8_t octet : v)
    {
        ss.width(2);
        ss.fill('0');
        ss << std::hex << (unsigned int)octet << ' ';
    }
    return ss.str();
}

// Checks HDLC frame's FCS and returns true in case it is valid
bool validateFcs( const FrameIterator startOfHdlcFrame, const FrameIterator endOfHdlcFrame)
{

	// How to check the HDLC checksum?
	std::vector<uint8_t> correctTrailer (HDLC_FRAME_TRAILER_SIZE, 0);
	felix::hdlc::encode_hdlc_trailer(
			&correctTrailer.at(0),
			&*startOfHdlcFrame,
			std::distance(startOfHdlcFrame, endOfHdlcFrame) - HDLC_FRAME_TRAILER_SIZE);

	auto wrongChecksumIndicator = std::mismatch(
			std::begin(correctTrailer),
			std::end(correctTrailer),
			startOfHdlcFrame + std::distance(startOfHdlcFrame, endOfHdlcFrame) - HDLC_FRAME_TRAILER_SIZE);

	if (wrongChecksumIndicator.first != std::end(correctTrailer)) return false;
	return true;

}

// Checks HDLC frame's FCS and returns true in case it is valid
bool validateFcs( const std::vector<uint8_t>& hdlcFrame )
{

	// How to check the HDLC checksum?
	std::vector<uint8_t> correctTrailer (HDLC_FRAME_TRAILER_SIZE, 0);
	felix::hdlc::encode_hdlc_trailer(
			&correctTrailer.at(0),
			&hdlcFrame.at(0),
			hdlcFrame.size() - HDLC_FRAME_TRAILER_SIZE);

	auto wrongChecksumIndicator = std::mismatch(
			std::begin(correctTrailer),
			std::end(correctTrailer),
			std::begin(hdlcFrame) + hdlcFrame.size() - HDLC_FRAME_TRAILER_SIZE);

	if (wrongChecksumIndicator.first != std::end(correctTrailer)) return false;
	return true;

}

}
